# Flaskr: Blogging made Simple
Flaskr is a simple blogging web app with features like login, register, create, update and delete posts. It is made with flask(python web framework), python and sqlite3.

### Installation
1. Python 3.6 or above recommended.
2. Clone/Download the project.
3. Move to the repository via `cd flaskr_blog`.
4. Install the project by running `pip install -e`.

### Credits
This project is copied from Flask Tutorial 1.0. Check it out [here](http://flask.pocoo.org/docs/1.0/tutorial)
