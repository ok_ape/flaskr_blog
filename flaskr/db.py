"""
    App: Flasker Blog App
        DATABASE METHODS
    author: Amrit Pandey @ap4tt
    copyrights: https://flask.pocoo.org
"""

import sqlite3

import click
from flask import current_app, g
from flask.cli import with_appcontext

# Connect db
def get_db():
    if 'db' not in g:
        g.db = sqlite3.connect(
            current_app.config["DATABASE"],
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.db.row_factory = sqlite3.Row

    return g.db

# Close db after connection
def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()

# Initialise db based on schema.sql
def init_db():
    db = get_db()

    with current_app.open_resource('schema.sql') as f:
        db.executescript(f.read().decode('utf8'))

# Command line function to initialse a db
# Run this command `init-db` to create and
# initialise the app.
@click.command('init-db')
@with_appcontext
def init_db_command():
    init_db()
    click.echo('Initialized the database.')

def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)