"""
    App: Flasker Blog App
    author: Amrit Pandey @ap4tt
    copyrights: https://flask.pocoo.org
"""

import os

# Import Flask class
from flask import Flask


# We are not creating an instance of Flask
# instead we are wrapping it in a function
# so that any change to app config will happen
# inside this function, which will then return 
# the app.
def create_app(test_config=None):
    # initialse app
    app = Flask(__name__, instance_relative_config=True)

    # initialse a database when called for
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # check for instance folder
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # a simple page that says hello
    @app.route('/hello')
    def hello():
        return 'Hello, World!'

    # resgister database
    from . import db
    db.init_app(app)

    # register authentication bp
    from . import auth
    app.register_blueprint(auth.bp)

    # register blog bp
    from . import blog
    app.register_blueprint(blog.bp)
    app.add_url_rule('/', endpoint="index")

    # Any change in config of app is now returned
    return app