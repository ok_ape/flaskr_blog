"""
    App: Flasker Blog App
        AUTHENTICATION BLUEPRINT
    author: Amrit Pandey @ap4tt
    copyrights: https://flask.pocoo.org
"""

import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)

from werkzeug.security import check_password_hash, generate_password_hash

from flaskr.db import get_db

# auth blueprint
bp = Blueprint('auth', __name__, url_prefix='/auth')

# route for /auth/register
@bp.route('/register', methods=('GET', 'POST'))
def register():
    if request.method == "POST":
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        error = None

        # check for username
        if not username:
            error = 'Username is required'
        # check for password
        elif not password:
            error = 'Password is required'
        # check if username already exist
        elif db.execute(
            'SELECT id FROM user WHERE username = ?', (username, )
        ).fetchone() is not None:
            error = 'User {} is already registered'.format(username)
        
        # no errors, proceed
        if error is None:
            db.execute(
                'INSERT INTO user (username, password) VALUES (?, ?)',
                (username, generate_password_hash(password))
            )
            db.commit()
            # redirect to login page after success registeration
            return redirect(url_for('auth.login'))
        # flash displays errors, if there are any.
        flash(error)
    # display registeration page 
    return render_template('auth/register.html')

# route for /auth/login
@bp.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        error = None
        # check for user
        user = db.execute(
            'SELECT * FROM user WHERE username = ?', (username, )
        ).fetchone()
        if user is None:
            error = 'Incorrect username'
        elif not check_password_hash(user['password'], password):
            error = 'Incorrect password'

        # proceed with no errors
        if error is None:
            # Clear old session storage.
            session.clear()
            # Saving user id in session storage,
            # this id will be used by the app
            # to use user info on every view.
            session['user_id'] = user['id']
            # redirect to blog after success login
            return redirect(url_for('index'))
        
        flash(error)
    # display login page
    return render_template('auth/login.html')

# route for /auth/logout
@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))

# This function run before page makes any requests
# to check if user is already logged in the session.
@bp.before_app_request
def load_logged_in_user():

    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        g.user = get_db().execute(
            'SELECT * FROM user WHERE id=?', (user_id, )
        ).fetchone()

# a simple method that redirects users to login page, if required.
def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view